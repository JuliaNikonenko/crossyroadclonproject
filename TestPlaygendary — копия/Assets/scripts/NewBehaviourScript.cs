﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour
{
    bool isJumpingUp;
    bool isJumpingDown;
    bool isJumpingRight;
    bool isJumpingLeft;

    public GameObject strip1;
    public GameObject strip2;
    public GameObject strip3;
    public GameObject strip4;
    public GameObject strip5;
    public GameObject strip6;
    public GameObject strip7;
    public GameObject strip8;
    public GameObject strip9;
    public GameObject strip10;
    public GameObject strip11;
    public GameObject strip12;
    public GameObject strip13;

    private List<GameObject> strips;
    int stripsCurrentIndex;
    //to dynemic road generated
    public GameObject [] poolStripsPrefabs;

    float jumpOffSetX=1.5f;
    public Vector3 JumpTargetLocation;
    public float movingSpeed = 100.0f;

    //to jump to the right/left
    public float jumpDistanceZ = 7.0f;

    //heightjump 
    private float midWayPointX;
    public float jumHeightIncrement = 20.0f;
    //to not fall in Y
    private float initialPositionY;

    //for death animation
    private bool isDead = false;
    private bool isPlayDeathAnimation = false;
    public GameObject mesh;

    public GameObject BoundaryLeft;
    public GameObject BoundaryRight;

    public int score = 0;
    public int indexHighestStrip = 0;
    public Text scoreText;
    bool gameIsPlaying = false;
    public GameObject startPanel;
    public GameObject gameOverPanel;

    public AudioClip pickUpCoinSound;
    public AudioClip deathSound;
    public AudioClip buttonPressed;

    
    void Start()
    {
        strips = new List<GameObject>();
        isJumpingUp = false;
        isJumpingDown = false;
        isJumpingRight = false;
        isJumpingLeft = false;

        strips.Add (strip1);
        strips.Add (strip2);
        strips.Add (strip3);
        strips.Add (strip4);
        strips.Add (strip5);
        strips.Add (strip6);
        strips.Add (strip7);
        strips.Add (strip8);
        strips.Add (strip9);
        strips.Add (strip10);
        strips.Add (strip11);
        strips.Add (strip12);
        strips.Add (strip13);

        stripsCurrentIndex =0;
        initialPositionY = this.transform.position.y;
        HideGameOverPanel();

        int isGameReloaded = PlayerPrefs.GetInt("reloaded");
        if (isGameReloaded == 1)
        {
            PlayerPrefs.SetInt("reloaded", 0); 
            ButtonStartPressed();
        }
    }

    void Update()
    {
        if(gameIsPlaying == false)
        {
            return;
        }
        if(isDead == true)
        {
            return;
        }
        // if(Input.GetMouseButtonDown(0))
        // {
        //     if(isJumping == false)
        //     {
        //         isJumping = true;
        //         Jump();
        //     }
        // }
        if(isJumpingUp) {
            if(this.transform.position.x > JumpTargetLocation.x) {
                this.transform.position = new Vector3(this.transform.position.x - (movingSpeed * Time.deltaTime), this.transform.position.y, this.transform.position.z);
                if(this.transform.position.x > midWayPointX) {
                    //calculate jump height 
                    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + jumHeightIncrement * Time.deltaTime, this.transform.position.z);
                }
                else {
                    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y - jumHeightIncrement * Time.deltaTime, this.transform.position.z);
                }
            }
            else {
                isJumpingUp = false;
                this.transform.position = new Vector3(transform.position.x, initialPositionY, transform.position.z);
            }
        }

        else if(isJumpingDown) 
        {
            if(this.transform.position.x < JumpTargetLocation.x) {
                this.transform.position = new Vector3(this.transform.position.x + (movingSpeed * Time.deltaTime), this.transform.position.y, this.transform.position.z);
                if(this.transform.position.x < midWayPointX) {
                    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + jumHeightIncrement * Time.deltaTime, this.transform.position.z);
                }
                else {
                    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y - jumHeightIncrement * Time.deltaTime, this.transform.position.z);
                }
            }
            else {
                isJumpingDown = false;
                this.transform.position = new Vector3(transform.position.x, initialPositionY, transform.position.z);
            }
        }
        else if(isJumpingRight){
            if(this.transform.position.z < JumpTargetLocation.z) {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + (movingSpeed * Time.deltaTime));
                if(this.transform.position.z < midWayPointX) {
                    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + jumHeightIncrement * Time.deltaTime, this.transform.position.z);
                }
                else {
                    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y - jumHeightIncrement * Time.deltaTime, this.transform.position.z);
                }
            }
            else {
                isJumpingRight = false;
                this.transform.position = new Vector3(transform.position.x, initialPositionY, transform.position.z);
            }
        }
        else if(isJumpingLeft) {
            if(this.transform.position.z > JumpTargetLocation.z) {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - (movingSpeed * Time.deltaTime));
                if(this.transform.position.z > midWayPointX) {
                    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + jumHeightIncrement * Time.deltaTime, this.transform.position.z);
                }
                else {
                    this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y - jumHeightIncrement * Time.deltaTime, this.transform.position.z);
                }
            }
            else {
                isJumpingLeft = false;
                this.transform.position = new Vector3(transform.position.x, initialPositionY, transform.position.z);
            }
        }


        if(isPlayDeathAnimation == true)
        {
            UpdateDeathAnimation();
        }
    }

    void JumpUp()
    {
        if(gameIsPlaying==false)
        {
            return;
        }
        stripsCurrentIndex+=1;
        if(stripsCurrentIndex > indexHighestStrip)
        {
            score += 10;
            scoreText.text ="SCORE:"+ score.ToString();
            indexHighestStrip = stripsCurrentIndex;
            Debug.Log("new score"+indexHighestStrip.ToString());
        }
        GameObject nextStrip = strips [stripsCurrentIndex] as GameObject;
        JumpTargetLocation = new Vector3 (nextStrip.transform.position.x - jumpOffSetX, transform.position.y, transform.position.z);
        midWayPointX = JumpTargetLocation.x + ((this.transform.position.x - JumpTargetLocation.x)/2);
        mesh.transform.localEulerAngles = new Vector3(0, 0, 0);

        //to create add road when we jump
        SpawnNewStrip();
        //to dynemicaly create boundaries
        float distanceX = this.transform.position.x - JumpTargetLocation.x;
        BoundaryLeft.transform.position -= new Vector3(distanceX, 0, 0);
        BoundaryRight.transform.position -= new Vector3(distanceX, 0, 0);
    }

    void JumpDown()
    {
        if (gameIsPlaying == false)
        {
            return;
        }
        stripsCurrentIndex -= 1;
        if (stripsCurrentIndex < 0)
        {
            stripsCurrentIndex = 0;
            return;
        }
        GameObject previousStrip = strips[stripsCurrentIndex] as GameObject;
        JumpTargetLocation = new Vector3(previousStrip.transform.position.x - jumpOffSetX, transform.position.y, transform.position.z);
        midWayPointX = JumpTargetLocation.x - ((JumpTargetLocation.x - this.transform.position.x) / 2);
        mesh.transform.localEulerAngles = new Vector3(0, 180, 0);

        float distanceX = JumpTargetLocation.x - this.transform.position.x;
        BoundaryLeft.transform.position += new Vector3(distanceX, 0, 0);
        BoundaryRight.transform.position += new Vector3(distanceX, 0, 0);
    }

    void JumpRight()
    {
        if (gameIsPlaying == false)
        {
            return;
        }
        JumpTargetLocation = new Vector3(transform.position.x, transform.position.y, transform.position.z + jumpDistanceZ);
        midWayPointX = JumpTargetLocation.z + ((this.transform.position.z - JumpTargetLocation.z) / 2);
        mesh.transform.localEulerAngles = new Vector3(0, 90, 0);
    }

    void JumpLeft()
    {
        if (gameIsPlaying == false)
        {
            return;
        }
        //JumpDistanceZ it is Offset
        JumpTargetLocation = new Vector3(transform.position.x, transform.position.y, transform.position.z - jumpDistanceZ);
        midWayPointX = JumpTargetLocation.z - ((JumpTargetLocation.z - this.transform.position.z) / 2);
        mesh.transform.localEulerAngles = new Vector3(0, -90, 0);
    }

    void SwipeUp()
    {
        if (isJumpingUp == false)
        {
            isJumpingUp = true;
            JumpUp();
        }
    }
    void SwipeDown()
    {
        if (isJumpingDown == false)
        {
            isJumpingDown = true;
            JumpDown();
        }
    }
    void SwipeRight()
    {
        if (isJumpingRight == false)
        {
            isJumpingRight = true;
            JumpRight();
        }
    }
    void SwipeLeft()
    {
        if (isJumpingLeft == false)
        {
            isJumpingLeft = true;
            JumpLeft();
        }
    }

    void SpawnNewStrip()
    {
        //take a road from pool of strips in prefab at random
        //add it to the last current strip add the width of this item as an offset
        int stripsPrefabCount = poolStripsPrefabs.Length;
        int randomNumber = Random.Range (0, stripsPrefabCount);
        GameObject item = poolStripsPrefabs[randomNumber] as GameObject;
        //get the width of the item
        Transform itemChildTransform = item.transform.GetChild(0) as Transform;
        Transform itemChildOfChildTransform = itemChildTransform.GetChild(0) as Transform;
        float itemWidth = itemChildOfChildTransform.gameObject.GetComponent<Renderer>().bounds.size.x;
        //get location of the last item
        GameObject lastStrip = strips [strips.Count -1] as GameObject;
        GameObject newStrip = Instantiate(item,lastStrip.transform.position, lastStrip.transform.rotation) as GameObject;
        newStrip.transform.position = new Vector3(newStrip.transform.position.x - itemWidth, newStrip.transform.position.y, newStrip.transform.position.z);
        strips.Add (newStrip);
    }

    void OnTriggerEnter(Collider other)
    { 
        if(other.gameObject.tag == "ENEMY")
        {
            DeathAnimation();
        }
        if(other.gameObject.tag == "obstacle")
        {
            float offsetUpDown = 0;
            float offsetLeftRight = 0;
            if(isJumpingDown) { offsetUpDown = -2.0f; }
            else if(isJumpingUp) {offsetUpDown = 2.0f;}
            else if(isJumpingRight){offsetLeftRight = 2.0f;}
            else if(isJumpingLeft){offsetLeftRight = -2.0f;}
            transform.position = new Vector3(transform.position.x + offsetUpDown, initialPositionY, transform.position.z + offsetLeftRight);
            isJumpingUp = isJumpingDown = isJumpingLeft = isJumpingRight = false;
        }
        if(other.gameObject.tag == "coin")
        {
            score+=10;
            scoreText.text = "SCORE:"+score.ToString();
            this.GetComponent<AudioSource>().PlayOneShot(pickUpCoinSound);
            Destroy(other.gameObject);
        }
    }

    void DeathAnimation()
    {
        isPlayDeathAnimation = true;
    }

    void UpdateDeathAnimation()
    {
        if(mesh.transform.localScale.z > 0.1)
        {
            mesh.transform.localScale -= new Vector3(0, 0, 0.02f);
        }
        else
        {
            isPlayDeathAnimation = false;
            isDead = true;
            this.GetComponent<AudioSource>().PlayOneShot(deathSound);
            BringUpGameOverPanel();
        }
        if(mesh.transform.rotation.eulerAngles.x == 0 || mesh.transform.rotation.eulerAngles.x > 270)
        {
            mesh.transform.Rotate(-4.0f, 9.8f, 0);
        }
    }

    void ButtonStartPressed ()
    {
        Debug.Log("button start pressed");
        this.GetComponent<AudioSource>().PlayOneShot(buttonPressed);
        gameIsPlaying = true;
        startPanel.SetActive(false);
    }

    void BringUpGameOverPanel()
    {
        gameOverPanel.SetActive(true);
    }

    void HideGameOverPanel()
    {
        gameOverPanel.SetActive(false);
    }

    void PlayAgain()
    {
        PlayerPrefs.SetInt("reloaded", 1);
        Application.LoadLevel("Level_1");
    }
}











